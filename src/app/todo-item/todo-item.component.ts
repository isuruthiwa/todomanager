import { Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {ToDo} from '../todo';

@Component({
  selector: 'todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {
  

  @Input()
  toDo: ToDo;

  @Output()
  removeItem= new EventEmitter();

  @Output()
  completeItem= new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  completedToDo(){
    this.completeItem.emit(this.toDo);
  }

  removeToDo(){
    this.removeItem.emit(this.toDo);
  }
}
