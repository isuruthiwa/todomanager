import { Component, OnInit, ViewChild } from '@angular/core';
import {ToDo} from '../todo';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  toDos:ToDo[]= [
    {
      name:"Task 1",
      status: false
    },
    {
      name:"Task 2",
      status: true
    },
    {
      name:"Task 3",
      status: false
    }
  ];

  regexp = new RegExp('^[1-9]\d{0,2}$');

  inputToDo: Text;
  errors: string;

  constructor() { 
  }

  ngOnInit() {
  }

  removeItem(removeToDo){
    const index= this.toDos.indexOf(removeToDo);
    if(index>-1){
      this.toDos.splice(index,1);
    }
  }

  completeItem(completedItem){
    const index= this.toDos.indexOf(completedItem);
    if(index>-1){
      this.toDos[index].status=true;
    }
  }

  public addToDo(inputToDo: string, form: NgForm){
    this.clearError();
    if(inputToDo!=null && inputToDo!=""){
      this.toDos.push({
        name: inputToDo,
        status: false
      });
      form.resetForm(); 
    }
    else{
      this.printError();
    }
  }

  private printError(){
    this.errors="Input text error : can not be empty";
  }

  private clearError(){
    this.errors="";
  }
}
